package Model;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Inventory {
    private ObservableList<Part> allParts;
    private ObservableList<Product> products;

    public Inventory() {
        this.allParts = new SimpleListProperty<>(FXCollections.observableArrayList());
        this.products = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public void addPart(Part part) {
        this.allParts.add(part);
    }

    public boolean removePart(int partID) {
        return this.allParts.remove(lookupPart(partID));
    }

    public Part lookupPart(int partID) {
        return this.allParts.filtered(part -> part.getPartID() == partID).get(0);
    }

    // Instructions said to create updatePart(int) but
    // this doesn't make any sense; how do we update a part with
    // just the ID and no information to give it??
    public void updatePart(Part part) {
        removePart(part.getPartID());
        addPart(part);
    }

    public ObservableList<Part> getAllParts() {
        return this.allParts;
    }


    public void addProduct(Product product) {
        this.products.add(product);
    }

    // should probably make sure this does a deep delete if orphaning the product
    // object doesnt do that alread
    public boolean removeProduct(int productID) {
        return this.products.remove(lookupProduct(productID));
    }

    public Product lookupProduct(int productID) {
        return this.products.filtered(product -> product.getProductID() == productID).get(0);
    }

    // Instructions said to create updateProduct(int) but
    // this doesn't make any sense; how do we update a product with
    // just the ID and no information to give it??
    public void updateProduct(Product product) {
        removeProduct(product.getProductID());
        addProduct(product);
    }

    public ObservableList<Product> getProducts() {
        return this.products;
    }
}