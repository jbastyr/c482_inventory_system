package Model;

import javafx.beans.property.*;

public abstract class Part {

    private IntegerProperty partID;
    private StringProperty name;
    private DoubleProperty price;
    private IntegerProperty inStock;
    private IntegerProperty min;
    private IntegerProperty max;

    public Part() {
        this.partID = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
        this.price = new SimpleDoubleProperty();
        this.inStock = new SimpleIntegerProperty();
        this.min = new SimpleIntegerProperty();
        this.max = new SimpleIntegerProperty();
    }

    public final String getName() {
        return this.name.get();
    }

    public final void setName(String name) {
        this.name.set(name);
    }

    public final double getPrice() {
        return this.price.get();
    }

    public final void setPrice(double price) {
        this.price.set(price);
    }

    public final int getInStock() {
        return this.inStock.get();
    }

    public final void setInStock(int inStock) {
        this.inStock.set(inStock);
    }

    public final int getMin() {
        return this.min.get();
    }

    public final void setMin(int min) {
        this.min.set(min);
    }

    public final int getMax() {
        return this.max.get();
    }

    public final void setMax(int max) {
        this.max.set(max);
    }

    public final int getPartID() {
        return this.partID.get();
    }

    public final void setPartID(int partID) {
        this.partID.set(partID);
    }

    public final IntegerProperty getPartIDProperty() {
        return this.partID;
    }

    public final StringProperty getNameProperty() {
        return this.name;
    }

    public final DoubleProperty getPriceProperty() {
        return this.price;
    }

    public final IntegerProperty getInStockProperty() {
        return this.inStock;
    }

    public final IntegerProperty getMinProperty() {
        return this.min;
    }

    public final IntegerProperty getMaxProperty() {
        return this.max;
    }
}
