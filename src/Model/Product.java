package Model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Product {
    private ObservableList<Part> associatedParts;
    private IntegerProperty productID;
    private StringProperty name;
    private DoubleProperty price;
    private IntegerProperty inStock;
    private IntegerProperty min;
    private IntegerProperty max;

    public Product() {
        this.associatedParts = new SimpleListProperty<>(FXCollections.observableArrayList());
        this.productID = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
        this.price = new SimpleDoubleProperty();
        this.inStock = new SimpleIntegerProperty();
        this.min = new SimpleIntegerProperty();
        this.max = new SimpleIntegerProperty();
    }

    public int getProductID() {
        return this.productID.get();
    }

    public void setProductID(int productID) {
        this.productID.set(productID);
    }

    public String getName() {
        return this.name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getPrice() {
        return this.price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public int getInStock() {
        return this.inStock.get();
    }

    public void setInStock(int inStock) {
        this.inStock.set(inStock);
    }

    public int getMin() {
        return this.min.get();
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public int getMax() {
        return this.max.get();
    }

    public void setMax(int max) {
        this.max.set(max);
    }

    public void addAssociatedPart(Part part) {
        this.associatedParts.add(part);
    }

    public boolean removeAssociatedPart(int partID) {
        return this.associatedParts.remove(lookupAssociatedPart(partID));
    }

    public Part lookupAssociatedPart(int partID) {
        return this.associatedParts.filtered(part -> part.getPartID() == partID).get(0);
    }

    public ObservableList<Part> getAssociatedParts() {
        return this.associatedParts;
    }
}