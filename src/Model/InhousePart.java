package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class InhousePart extends Part {
    private IntegerProperty machineID;

    public InhousePart() {
        super();
        this.machineID = new SimpleIntegerProperty();
    }

    public int getMachineID() {
        return this.machineID.get();
    }

    public void setMachineID(int machineID) {
        this.machineID.set(machineID);
    }
}
