/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeremybastyr_inventorysystem;

import Model.InhousePart;
import Model.OutsourcedPart;
import Model.Product;
import Service.AlertService;
import Service.InventoryService;
import Service.SceneService;
import Service.SequenceService;
import javafx.application.Application;
import javafx.stage.Stage;

public class JeremyBastyr_InventorySystem extends Application {
    private static SceneService sceneService;
    private static InventoryService inventoryService;
    private static SequenceService sequenceService;
    private static AlertService alertService;

    public static void main(String[] args) {
        instantiateServices();
        // Comment this next line to input all data manually.
        // Just a quick way to get info there
        addSampleData();
        launch(args);
    }

    private static void instantiateServices() {
        sceneService = SceneService.getInstance();
        inventoryService = InventoryService.getInstance();
        sequenceService = SequenceService.getInstance();
        alertService = AlertService.getInstance();
    }

    private static void addSampleData() {
        InhousePart part1 = new InhousePart();
        part1.setPartID(sequenceService.getSequenceID());
        part1.setName("Cool Part");
        part1.setInStock(10);
        part1.setMax(24);
        part1.setMin(1);
        part1.setPrice(5.00);
        part1.setMachineID(12694);

        OutsourcedPart part2 = new OutsourcedPart();
        part2.setPartID(sequenceService.getSequenceID());
        part2.setName("Really awesome part");
        part2.setInStock(40);
        part2.setMax(100);
        part2.setMin(3);
        part2.setPrice(0.25);
        part2.setCompanyName("Manu-fact Inc");

        Product product1 = new Product();
        product1.setProductID(sequenceService.getSequenceID());
        product1.setName("MegaProduct 1000");
        product1.setInStock(4);
        product1.setMax(12);
        product1.setMin(1);
        product1.setPrice(10.0);
        product1.addAssociatedPart(part1);
        product1.addAssociatedPart(part2);

        inventoryService.getInventory().addPart(part1);
        inventoryService.getInventory().addPart(part2);
        inventoryService.getInventory().addProduct(product1);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Inventory Management System - Jeremy Bastyr");
        stage.setOnCloseRequest(event -> {
            if (!alertService.getAlertConfirmResponse("Exit application", "Are you sure you want to quit?")) {
                event.consume();
            }
        });
        sceneService.LaunchScene(stage, getClass().getResource("/ViewController/Main.fxml"));
    }
}
