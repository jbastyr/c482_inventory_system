package ViewController;

import Exception.ValidateException;
import Model.InhousePart;
import Model.OutsourcedPart;
import Model.Part;
import Service.AlertService;
import Service.InventoryService;
import Service.SequenceService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PartController implements Initializable, DataAcceptor {

    private InventoryService inventoryService;
    private SequenceService sequenceService;
    private AlertService alertService;

    @FXML
    private RadioButton radioSourceOutsourced;
    @FXML
    private RadioButton radioSourceInHouse;
    @FXML
    private Label labelCompanyName;
    @FXML
    private BorderPane partRoot;
    @FXML
    private TextField fieldMachineID;
    @FXML
    private TextField fieldCompanyName;
    @FXML
    private Label labelMachineID;
    @FXML
    private TextField fieldId;
    @FXML
    private TextField fieldName;
    @FXML
    private TextField fieldInventory;
    @FXML
    private TextField fieldPriceCost;
    @FXML
    private TextField fieldMax;
    @FXML
    private TextField fieldMin;
    @FXML
    private ToggleGroup SourceGroup;

    public PartController() {
        instantiateServices();
    }

    private void instantiateServices() {
        this.inventoryService = InventoryService.getInstance();
        this.sequenceService = SequenceService.getInstance();
        alertService = AlertService.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    void onSave(ActionEvent event) {
        try {
            verifyContainsData();
            verifyInventory();
            verifyMinMax();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }

        boolean update = false;
        if (!this.fieldId.getText().isEmpty()) {
            update = true;
        }

        Part part = radioSourceInHouse.isSelected() ?
                new InhousePart()
                : new OutsourcedPart();
        if (update) {
            part.setPartID(Integer.parseInt(this.fieldId.getText()));
        } else {
            part.setPartID(sequenceService.getSequenceID());
        }
        part.setName(this.fieldName.getText());
        part.setInStock(Integer.parseInt(this.fieldInventory.getText()));
        part.setPrice(Double.parseDouble(this.fieldPriceCost.getText()));
        part.setMax(Integer.parseInt(this.fieldMax.getText()));
        part.setMin(Integer.parseInt(this.fieldMin.getText()));


        if (part instanceof InhousePart) {
            ((InhousePart) part).setMachineID(Integer.parseInt(this.fieldMachineID.getText()));
        } else {
            ((OutsourcedPart) part).setCompanyName(this.fieldCompanyName.getText());
        }

        if (update) {
            this.inventoryService.getInventory().updatePart(part);
        } else {
            this.inventoryService.getInventory().addPart(part);
        }

        closeWindow();
    }

    @FXML
    void onCancel(ActionEvent event) {
        if (!alertService.getAlertConfirmResponse("Cancel", "Are you sure you want to cancel?")) {
            return;
        }
        closeWindow();
    }

    @FXML
    void onSourceChangeInHouse(ActionEvent event) {
        checkSelected();
    }

    @FXML
    void onSourceChangeOutsourced(ActionEvent event) {
        checkSelected();
    }

    private void checkSelected() {
        if (radioSourceInHouse.isSelected()) {
            labelCompanyName.setVisible(false);
            fieldCompanyName.setVisible(false);
            labelMachineID.setVisible(true);
            fieldMachineID.setVisible(true);
        }
        if (radioSourceOutsourced.isSelected()) {
            labelCompanyName.setVisible(true);
            fieldCompanyName.setVisible(true);
            labelMachineID.setVisible(false);
            fieldMachineID.setVisible(false);
        }
    }

    private void closeWindow() {
        if (partRoot.getScene().getWindow() instanceof Stage) {
            ((Stage) partRoot.getScene().getWindow()).close();
        }
    }

    public void setData(Object data) {
        if (data instanceof Part) {
            Part part = (Part) data;
            setFields(part);
        }
    }

    private void setFields(Part part) {
        this.fieldId.setText(Integer.toString(part.getPartID()));
        this.fieldName.setText(part.getName());
        this.fieldInventory.setText(Integer.toString(part.getInStock()));
        this.fieldMax.setText(Integer.toString(part.getMax()));
        this.fieldMin.setText(Integer.toString(part.getMin()));
        this.fieldPriceCost.setText(Double.toString(part.getPrice()));
        if (part instanceof InhousePart) {
            this.fieldMachineID.setText(Integer.toString(((InhousePart) part).getMachineID()));
            this.radioSourceInHouse.setSelected(true);
        } else {
            this.fieldCompanyName.setText(((OutsourcedPart) part).getCompanyName());
            this.radioSourceOutsourced.setSelected(true);
        }
        checkSelected();
    }

    private void verifyContainsData() throws ValidateException {
        boolean badData =
                this.fieldName.getText().isEmpty() ||
                        this.fieldInventory.getText().isEmpty() ||
                        this.fieldMax.getText().isEmpty() ||
                        this.fieldMin.getText().isEmpty();

        boolean sourceSpecific = (radioSourceInHouse.isSelected() ?
                this.fieldMachineID.getText().isEmpty() :
                this.fieldCompanyName.getText().isEmpty());
        badData |= sourceSpecific;

        if (badData) {
            throw new ValidateException("Missing data", "Data is missing from one or more fields");
        }
    }

    private void verifyInventory() throws ValidateException {
        int inventory = Integer.parseInt(this.fieldInventory.getText());
        int min = Integer.parseInt(this.fieldMin.getText());
        int max = Integer.parseInt(this.fieldMax.getText());
        if (inventory < min || inventory > max || inventory < 0) {
            throw new ValidateException("Inventory error", "Inventory level must be between min and max");
        }
    }

    private void verifyMinMax() throws ValidateException {
        if (Integer.parseInt(this.fieldMin.getText()) > Integer.parseInt(this.fieldMax.getText())) {
            throw new ValidateException("Min/Max error", "Min must be less than max");
        }
    }
}