package ViewController;

import Exception.ValidateException;
import Model.Part;
import Model.Product;
import Service.AlertService;
import Service.InventoryService;
import Service.SequenceService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductController implements Initializable, DataAcceptor {

    private InventoryService inventoryService;
    private SequenceService sequenceService;
    private AlertService alertService;

    @FXML
    private BorderPane productRoot;

    @FXML
    private TextField fieldProductID;
    @FXML
    private TextField fieldProductName;
    @FXML
    private TextField fieldProductInventory;
    @FXML
    private TextField fieldProductPrice;
    @FXML
    private TextField fieldProductMax;
    @FXML
    private TextField fieldProductMin;

    @FXML
    private TableView<Part> tableAllParts;
    @FXML
    private TableColumn<Part, Integer> columnAllPartsID;
    @FXML
    private TableColumn<Part, String> columnAllPartsName;
    @FXML
    private TableColumn<Part, Integer> columnAllPartsInventory;
    @FXML
    private TableColumn<Part, Double> columnAllPartsPrice;
    @FXML
    private TextField fieldPartsSearchTerm;

    @FXML
    private TableView<Part> tableProductParts;
    @FXML
    private TableColumn<Part, Integer> columnProductPartsID;
    @FXML
    private TableColumn<Part, String> columnProductPartsName;
    @FXML
    private TableColumn<Part, Integer> columnProductPartsInventory;
    @FXML
    private TableColumn<Part, Double> columnProductPartsPrice;


    public ProductController() {
        instantiateServices();
    }

    private void instantiateServices() {
        this.inventoryService = InventoryService.getInstance();
        this.sequenceService = SequenceService.getInstance();
        alertService = AlertService.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bindTables();
        fillPartsTable();
    }

    @FXML
    void onSave(ActionEvent event) {
        try {
            verifyContainsData();
            verifyInventory();
            verifyMinMax();
            verifyProductHasParts();
            verifyPrice();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }

        boolean update = false;
        if (!this.fieldProductID.getText().isEmpty()) {
            update = true;
        }

        Product product = new Product();

        if (update) {
            product.setProductID(Integer.parseInt(this.fieldProductID.getText()));
        } else {
            product.setProductID(sequenceService.getSequenceID());
        }

        product.setName(this.fieldProductName.getText());
        product.setInStock(Integer.parseInt(this.fieldProductInventory.getText()));
        product.setPrice(Double.parseDouble(this.fieldProductPrice.getText()));
        product.setMax(Integer.parseInt(this.fieldProductMax.getText()));
        product.setMin(Integer.parseInt(this.fieldProductMin.getText()));

        for (Part part : tableProductParts.getItems()) {
            product.addAssociatedPart(part);
        }

        if (update) {
            this.inventoryService.getInventory().updateProduct(product);
        } else {
            this.inventoryService.getInventory().addProduct(product);
        }

        closeWindow();
    }

    @FXML
    void onCancel(ActionEvent event) {
        if (!alertService.getAlertConfirmResponse("Cancel", "Are you sure you want to cancel?")) {
            return;
        }
        closeWindow();
    }

    // could move to search service, pass items, term, return item list
    @FXML
    void onSearch(ActionEvent event) {
        if (!this.fieldPartsSearchTerm.getText().isEmpty()) {
            String term = this.fieldPartsSearchTerm.getText();
            this.tableAllParts.setItems(this.tableAllParts.getItems().filtered(part ->
                    Integer.toString(part.getPartID()).contains(term) ||
                            part.getName().toUpperCase().contains(term.toUpperCase())
            ));
        } else {
            this.tableAllParts.setItems(this.inventoryService.getInventory().getAllParts());
        }
    }

    @FXML
    void onPartAdd(ActionEvent event) {
        try {
            verifyAllPartsPartSelected();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }
        Part part = this.tableAllParts.getSelectionModel().getSelectedItem();
        this.tableProductParts.getItems().add(part);
    }

    @FXML
    void onPartDelete(ActionEvent event) {
        try {
            verifyProductPartSelected();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }
        if (!alertService.getAlertConfirmResponse("Delete", "Are you sure you want to delete this item?")) {
            return;
        }
        Part part = this.tableProductParts.getSelectionModel().getSelectedItem();
        this.tableProductParts.getItems().remove(part);
    }

    private void setFields(Product product) {
        this.fieldProductID.setText(Integer.toString(product.getProductID()));
        this.fieldProductName.setText(product.getName());
        this.fieldProductInventory.setText(Integer.toString(product.getInStock()));
        this.fieldProductMax.setText(Integer.toString(product.getMax()));
        this.fieldProductMin.setText(Integer.toString(product.getMin()));
        this.fieldProductPrice.setText(Double.toString(product.getPrice()));
    }

    public void setData(Object data) {
        if (data instanceof Product) {
            Product product = (Product) data;
            setFields(product);
            fillProductPartsTable(product);
        }
    }

    private void closeWindow() {
        if (productRoot.getScene().getWindow() instanceof Stage) {
            ((Stage) productRoot.getScene().getWindow()).close();
        }
    }

    // could move to tableService, take column and property value, set valueFactory
    private void bindTables() {
        columnAllPartsID.setCellValueFactory(new PropertyValueFactory<>("partID"));
        columnAllPartsName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnAllPartsInventory.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        columnAllPartsPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

        columnProductPartsID.setCellValueFactory(new PropertyValueFactory<>("partID"));
        columnProductPartsName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnProductPartsInventory.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        columnProductPartsPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    private void fillPartsTable() {
        tableAllParts.setItems(inventoryService.getInventory().getAllParts());
    }

    private void fillProductPartsTable(Product product) {
        tableProductParts.setItems(product.getAssociatedParts());
    }

    private void verifyContainsData() throws ValidateException {
        boolean badData =
                this.fieldProductName.getText().isEmpty() ||
                        this.fieldProductInventory.getText().isEmpty() ||
                        this.fieldProductMax.getText().isEmpty() ||
                        this.fieldProductMin.getText().isEmpty();

        if (badData) {
            throw new ValidateException("Missing data", "Data is missing from one or more fields");
        }
    }

    private void verifyInventory() throws ValidateException {
        int inventory = Integer.parseInt(this.fieldProductInventory.getText());
        int min = Integer.parseInt(this.fieldProductMin.getText());
        int max = Integer.parseInt(this.fieldProductMax.getText());
        if (inventory < min || inventory > max || inventory < 0) {
            throw new ValidateException("Inventory error", "Inventory level must be between min and max, and at least 0");
        }
    }

    private void verifyMinMax() throws ValidateException {
        if (Integer.parseInt(this.fieldProductMin.getText()) > Integer.parseInt(this.fieldProductMax.getText())) {
            throw new ValidateException("Min/Max error", "Min must be less than max");
        }
    }

    private void verifyProductHasParts() throws ValidateException {
        if (this.tableProductParts.getItems().isEmpty()) {
            throw new ValidateException("Missing parts", "Product must have at least one part");
        }
    }

    private void verifyPrice() throws ValidateException {
        double partPriceSum = this.tableProductParts.getItems().stream().mapToDouble(Part::getPrice).sum();
        if (Double.parseDouble(this.fieldProductPrice.getText()) < partPriceSum) {
            throw new ValidateException("Bad price", "Product price must be more than the collective price of all parts");
        }
    }

    private void verifyAllPartsPartSelected() throws ValidateException {
        if (this.tableAllParts.getSelectionModel().getSelectedItem() == null) {
            throw new ValidateException("No part selected", "You must select a part in order to add it");
        }
    }

    private void verifyProductPartSelected() throws ValidateException {
        if (this.tableProductParts.getSelectionModel().getSelectedItem() == null) {
            throw new ValidateException("No part selected", "You must select a part in order to delete it");
        }
    }
}