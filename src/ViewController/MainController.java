package ViewController;

import Exception.ValidateException;
import Model.Part;
import Model.Product;
import Service.AlertService;
import Service.InventoryService;
import Service.SceneService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;


public class MainController implements Initializable {
    @FXML
    private BorderPane mainRoot;

    @FXML
    private TableView<Part> tableParts;
    @FXML
    private TableColumn<Part, Integer> columnPartID;
    @FXML
    private TableColumn<Part, String> columnPartName;
    @FXML
    private TableColumn<Part, Integer> columnPartInventory;
    @FXML
    private TableColumn<Part, Double> columnPartPrice;
    @FXML
    private TextField fieldPartsSearchTerm;

    @FXML
    private TableView<Product> tableProducts;
    @FXML
    private TableColumn<Product, Integer> columnProductID;
    @FXML
    private TableColumn<Product, String> columnProductName;
    @FXML
    private TableColumn<Product, Integer> columnProductInventory;
    @FXML
    private TableColumn<Product, Double> columnProductPrice;
    @FXML
    private TextField fieldProductsSearchTerm;

    private SceneService sceneService;
    private InventoryService inventoryService;
    private AlertService alertService;

    public MainController() {
        instantiateServices();
    }

    private void instantiateServices() {
        this.sceneService = SceneService.getInstance();
        this.inventoryService = InventoryService.getInstance();
        this.alertService = AlertService.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bindTables();
        fillTables();
    }

    @FXML
    void onExit(ActionEvent event) {
        // easier than checking event for button
        if (!alertService.getAlertConfirmResponse("Exit application", "Are you sure you want to quit?")) {
            return;
        }
        if (mainRoot.getScene().getWindow() instanceof Stage) {
            ((Stage) mainRoot.getScene().getWindow()).close();
        }
    }

    @FXML
    void onAddPart(ActionEvent event) {
        sceneService.LaunchScene(getClass().getResource("Part.fxml"),
                "Add new Part",
                true);
    }

    @FXML
    void onModifyPart(ActionEvent event) {
        try {
            verifyPartSelected();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }
        Part part = this.tableParts.getSelectionModel().getSelectedItem();
        sceneService.LaunchScene(getClass().getResource("Part.fxml"),
                "Modify Part",
                true,
                part);
    }

    @FXML
    void onDeletePart(ActionEvent event) {
        try {
            verifyPartSelected();
            verifyPartUnassociated();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }

        if (!alertService.getAlertConfirmResponse("Delete", "Are you sure you want to delete this item?")) {
            return;
        }
        int partId = this.tableParts.getSelectionModel().getSelectedItem().getPartID();
        inventoryService.getInventory().removePart(partId);
    }

    // could move to search service, pass items, term, return item list
    @FXML
    void onSearchParts(ActionEvent event) {
        if (!this.fieldPartsSearchTerm.getText().isEmpty()) {
            String term = this.fieldPartsSearchTerm.getText();
            this.tableParts.setItems(this.tableParts.getItems().filtered(part ->
                    Integer.toString(part.getPartID()).contains(term) ||
                            part.getName().toUpperCase().contains(term.toUpperCase())
            ));
        } else {
            this.tableParts.setItems(this.inventoryService.getInventory().getAllParts());
        }
    }

    @FXML
    void onAddProduct(ActionEvent event) {
        sceneService.LaunchScene(getClass().getResource("Product.fxml"),
                "Add new Product",
                true);
    }

    @FXML
    void onModifyProduct(ActionEvent event) {
        try {
            verifyProductSelected();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }
        Product product = this.tableProducts.getSelectionModel().getSelectedItem();
        sceneService.LaunchScene(getClass().getResource("Product.fxml"),
                "Modify Product",
                true,
                product);
    }

    @FXML
    void onDeleteProduct(ActionEvent event) {
        try {
            verifyProductSelected();
        } catch (ValidateException ex) {
            alertService.showAlertError(ex.getHeader(), ex.getMessage());
            return;
        }

        if (!alertService.getAlertConfirmResponse("Delete", "Are you sure you want to delete this item?")) {
            return;
        }

        int productId = this.tableProducts.getSelectionModel().getSelectedItem().getProductID();
        inventoryService.getInventory().removeProduct(productId);
    }

    // could move to search service, pass items, term, return item list
    @FXML
    void onSearchProducts(ActionEvent event) {
        if (!this.fieldProductsSearchTerm.getText().isEmpty()) {
            String term = this.fieldProductsSearchTerm.getText();
            this.tableProducts.setItems(this.tableProducts.getItems().filtered(part ->
                    Integer.toString(part.getProductID()).contains(term) ||
                            part.getName().toUpperCase().contains(term.toUpperCase())
            ));
        } else {
            // refill table if empty search
            this.tableProducts.setItems(this.inventoryService.getInventory().getProducts());
        }
    }


    // could move to tableService, take column and property value, set factory
    private void bindTables() {
        columnPartID.setCellValueFactory(new PropertyValueFactory<>("partID"));
        columnPartName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnPartInventory.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        columnPartPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

        columnProductID.setCellValueFactory(new PropertyValueFactory<>("productID"));
        columnProductName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnProductInventory.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        columnProductPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    private void fillTables() {
        tableParts.setItems(inventoryService.getInventory().getAllParts());
        tableProducts.setItems(inventoryService.getInventory().getProducts());
    }

    private void verifyPartSelected() throws ValidateException {
        if (this.tableParts.getSelectionModel().getSelectedItem() == null) {
            throw new ValidateException("No part selected", "You must select a part in order to modify or delete it");
        }
    }

    private void verifyProductSelected() throws ValidateException {
        if (this.tableProducts.getSelectionModel().getSelectedItem() == null) {
            throw new ValidateException("No product selected", "You must select a product in order to modify or delete it");
        }

    }

    private void verifyPartUnassociated() throws ValidateException {
        Part part = this.tableParts.getSelectionModel().getSelectedItem();
        if (inventoryService.getInventory().getProducts().filtered(product ->
                product.getAssociatedParts().contains(part)).isEmpty()) {
            throw new ValidateException("Part associated", "This part belongs to a product. Delete it from the product before continuing");
        }
    }
}




