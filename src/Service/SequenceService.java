package Service;

public class SequenceService {
    private static SequenceService service = null;
    private static int sequenceID;

    private SequenceService() {
    }

    public static SequenceService getInstance() {
        if (service == null) {
            service = new SequenceService();
            sequenceID = 0;
        }
        return service;
    }

    public int getSequenceID() {
        return ++sequenceID;
    }

}
