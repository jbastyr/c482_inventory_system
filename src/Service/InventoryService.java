package Service;

import Model.Inventory;

public class InventoryService {

    private static InventoryService service = null;
    private static Inventory inventory = null;

    private InventoryService() {
    }

    public static InventoryService getInstance() {
        if (service == null) {
            service = new InventoryService();
            inventory = new Inventory();
        }

        return service;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
