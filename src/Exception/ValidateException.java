package Exception;

public class ValidateException extends Exception {

    private String header = "Exception";
    private String message = "Message";

    public ValidateException() {
        super();
    }

    public ValidateException(String header, String message) {
        super();
        this.header = header;
        this.message = message;
    }

    public String getHeader() {
        return header;
    }

    public String getMessage() {
        return message;
    }

}
